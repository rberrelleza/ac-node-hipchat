var assert = require('assert');
var Builder = require('..').Builder;

describe('ack hipchat builder', function () {

  describe('#key', function () {

    it('should accept a key value', function (done) {
      var descriptor;
      var builder = Builder({}, {
        addWebhook: function () {},
        ready: function (built) {
          descriptor = built;
          assert.equal(descriptor.key, 'test-key');
          done();
        }
      });
      builder.key('test-key');
    });

  });

  describe('#name', function () {

    it('should accept a name value', function (done) {
      var descriptor;
      var builder = Builder({}, {
        addWebhook: function () {},
        ready: function (built) {
          descriptor = built;
          assert.equal(descriptor.name, 'Test Name');
          done();
        }
      });
      builder.name('Test Name');
    });

  });

  describe('#description', function () {

    it('should accept a description value', function (done) {
      var descriptor;
      var builder = Builder({}, {
        addWebhook: function () {},
        ready: function (built) {
          descriptor = built;
          assert.equal(descriptor.description, 'Test description');
          done();
        }
      });
      builder.description('Test description');
    });

  });

  describe('#vendor', function () {

    it('should accept a vendor object', function (done) {
      var descriptor;
      var builder = Builder({}, {
        addWebhook: function () {},
        ready: function (built) {
          descriptor = built;
          assert.deepEqual(descriptor.vendor, {
            name: 'Vendor Name',
            url: 'http://example.com'
          });
          done();
        }
      });
      builder.vendor({
        name: 'Vendor Name',
        url: 'http://example.com'
      });
    });

  });

  describe('#vendorName', function () {

    it('should accept a vendor name', function (done) {
      var descriptor;
      var builder = Builder({}, {
        addWebhook: function () {},
        ready: function (built) {
          descriptor = built;
          assert.equal(descriptor.vendor.name, 'Vendor Name');
          done();
        }
      });
      builder.vendorName('Vendor Name');
    });

  });

  describe('#vendorUrl', function () {

    it('should accept a vendor url', function (done) {
      var descriptor;
      var builder = Builder({}, {
        addWebhook: function () {},
        ready: function (built) {
          descriptor = built;
          assert.equal(descriptor.vendor.url, 'http://example.com');
          done();
        }
      });
      builder.vendorUrl('http://example.com');
    });

  });

  describe('#avatarUrl', function () {

    it('should accept an avatar url', function (done) {
      var descriptor;
      var builder = Builder({}, {
        addWebhook: function () {},
        ready: function (built) {
          descriptor = built;
          assert.equal(descriptor.capabilities.hipchatApiConsumer.avatar.url, 'http://example.com/avatar.png');
          assert.equal(descriptor.capabilities.hipchatApiConsumer.avatar["url@2x"], 'http://example.com/avatar.png');
          done();
        }
      });
      builder.avatar('http://example.com/avatar.png');
    });

  });

  describe('#scopes', function () {

    it('should accept the param signature (scope)', function (done) {
      var descriptor;
      var builder = Builder({}, {
        addWebhook: function () {},
        ready: function (built) {
          descriptor = built;
          assert.deepEqual(descriptor.capabilities.hipchatApiConsumer.scopes, ['send_notification']);
          done();
        }
      });
      builder.scopes('send_notification');
    });

    it('should accept the param signature (scopes)', function (done) {
      var descriptor;
      var builder = Builder({}, {
        addWebhook: function () {},
        ready: function (built) {
          descriptor = built;
          assert.deepEqual(descriptor.capabilities.hipchatApiConsumer.scopes, ['send_notification', 'view_group']);
          done();
        }
      });
      builder.scopes(['send_notification', 'view_group']);
    });

    it('should accept the param signature (scope...)', function (done) {
      var descriptor;
      var builder = Builder({}, {
        addWebhook: function () {},
        ready: function (built) {
          descriptor = built;
          assert.deepEqual(descriptor.capabilities.hipchatApiConsumer.scopes, ['send_notification', 'view_group', 'admin_room']);
          done();
        }
      });
      builder.scopes('send_notification', 'view_group', 'admin_room');
    });

  });

  describe('#fromName', function () {

    it('should accept a fromName value', function (done) {
      var descriptor;
      var builder = Builder({}, {
        addWebhook: function () {},
        ready: function (built) {
          descriptor = built;
          assert.equal(descriptor.capabilities.hipchatApiConsumer.fromName, 'Test Name');
          done();
        }
      });
      builder.fromName('Test Name');
    });

  });

  describe('#allowGlobal', function () {

    it('should accept the param signature (boolean)', function (done) {
      var descriptor;
      var builder = Builder({}, {
        addWebhook: function () {},
        ready: function (built) {
          descriptor = built;
          assert.equal(descriptor.capabilities.installable.allowGlobal, true);
          done();
        }
      });
      builder.allowGlobal(true);
    });

  });

  describe('#allowRoom', function () {

    it('should accept the param signature (boolean)', function (done) {
      var descriptor;
      var builder = Builder({}, {
        addWebhook: function () {},
        ready: function (built) {
          descriptor = built;
          assert.equal(descriptor.capabilities.installable.allowRoom, true);
          done();
        }
      });
      builder.allowRoom(true);
    });

  });

  describe('#webhook', function () {

    it('should accept the param signature (event, url)', function () {
      var webhook, callback;
      var builder = Builder({}, {
        addWebhook: function (definition, cb) {
          webhook = definition;
          callback = cb;
        },
        ready: function () {}
      });
      builder.webhook('room_enter', '/room-enter');
      assert.equal(webhook.event, 'room_enter');
      assert.equal(webhook.url, '/room-enter');
      assert.ok(!callback);
    });

    it('should accept the param signature (event, callback)', function () {
      var webhook, callback;
      var builder = Builder({}, {
        addWebhook: function (definition, cb) {
          webhook = definition;
          callback = cb;
        },
        ready: function () {}
      });
      builder.webhook('room_enter', function *() {});
      assert.equal(webhook.event, 'room_enter');
      assert.ok(callback);
    });

    it('should reject the param signature (event, patternRe, url) when event != room_message', function () {
      var webhook, callback;
      var builder = Builder({}, {
        addWebhook: function (definition, cb) {
          webhook = definition;
          callback = cb;
        },
        ready: function () {}
      });
      try {
        builder.webhook('room_enter', /^\/foo(?:\s(.*)|$)/i, '/room-enter');
        assert.fail();
      } catch (e) {
        assert.ok(/pattern not supported/.test(e.message));
      }
    });

    it('should accept the param signature (event, patternRe, url)', function () {
      var webhook, callback;
      var builder = Builder({}, {
        addWebhook: function (definition, cb) {
          webhook = definition;
          callback = cb;
        },
        ready: function () {}
      });
      builder.webhook('room_message', /^\/foo(?:\s(.*)|$)/i, '/room-message');
      assert.equal(webhook.event, 'room_message');
      assert.equal(webhook.pattern.toString(), /^\/foo(?:\s(.*)|$)/i.toString());
      assert.equal(webhook.url, '/room-message');
      assert.ok(!callback);
    });

    it('should accept the param signature (event, patternRe, callback)', function () {
      var webhook, callback;
      var builder = Builder({}, {
        addWebhook: function (definition, cb) {
          webhook = definition;
          callback = cb;
        },
        ready: function () {}
      });
      builder.webhook('room_message', /^\/foo(?:\s(.*)|$)/i, function *() {});
      assert.equal(webhook.event, 'room_message');
      assert.equal(webhook.pattern.toString(), /^\/foo(?:\s(.*)|$)/i.toString());
      assert.ok(callback);
    });

    it('should accept the param signature (event, name, url)', function () {
      var webhook, callback;
      var builder = Builder({}, {
        addWebhook: function (definition, cb) {
          webhook = definition;
          callback = cb;
        },
        ready: function () {}
      });
      builder.webhook('room_enter', 'name', '/room-enter');
      assert.equal(webhook.event, 'room_enter');
      assert.equal(webhook.name, 'name');
      assert.equal(webhook.url, '/room-enter');
      assert.ok(!callback);
    });

    it('should accept the param signature (event, name, callback)', function () {
      var webhook, callback;
      var builder = Builder({}, {
        addWebhook: function (definition, cb) {
          webhook = definition;
          callback = cb;
        },
        ready: function () {}
      });
      builder.webhook('room_enter', 'name', function *() {});
      assert.equal(webhook.event, 'room_enter');
      assert.equal(webhook.name, 'name');
      assert.ok(callback);
    });

    it('should reject the param signature (event, name, patternRe, url) when event!=room_message', function () {
      var webhook, callback;
      var builder = Builder({}, {
        addWebhook: function (definition, cb) {
          webhook = definition;
          callback = cb;
        },
        ready: function () {}
      });
      try {
        builder.webhook('room_enter', 'name', /^\/foo(?:\s(.*)|$)/i, '/room-enter');
        assert.fail();
      } catch (e) {
        assert.ok(/pattern not supported/.test(e.message));
      }
    });

    it('should accept the param signature (event, name, patternRe, url)', function () {
      var webhook, callback;
      var builder = Builder({}, {
        addWebhook: function (definition, cb) {
          webhook = definition;
          callback = cb;
        },
        ready: function () {}
      });
      builder.webhook('room_message', 'name', /^\/foo(?:\s(.*)|$)/i, '/room-message');
      assert.equal(webhook.event, 'room_message');
      assert.equal(webhook.name, 'name');
      assert.equal(webhook.pattern.toString(), /^\/foo(?:\s(.*)|$)/i.toString());
      assert.equal(webhook.url, '/room-message');
      assert.ok(!callback);
    });

    it('should accept the param signature (event, name, patternRe, callback)', function () {
      var webhook, callback;
      var builder = Builder({}, {
        addWebhook: function (definition, cb) {
          webhook = definition;
          callback = cb;
        },
        ready: function () {}
      });
      builder.webhook('room_message', 'name', /^\/foo(?:\s(.*)|$)/i, function *() {});
      assert.equal(webhook.event, 'room_message');
      assert.equal(webhook.name, 'name');
      assert.equal(webhook.pattern.toString(), /^\/foo(?:\s(.*)|$)/i.toString());
      assert.ok(callback);
    });

    it('should accept the param signature (object)', function () {
      var webhook, callback;
      var builder = Builder({}, {
        addWebhook: function (definition, cb) {
          webhook = definition;
          callback = cb;
        },
        ready: function () {}
      });
      builder.webhook({
        event: 'room_message',
        name: 'name',
        pattern: /^\/foo(?:\s(.*)|$)/i,
        url: '/room-message'
      });
      assert.equal(webhook.event, 'room_message');
      assert.equal(webhook.name, 'name');
      assert.equal(webhook.pattern.toString(), /^\/foo(?:\s(.*)|$)/i.toString());
      assert.equal(webhook.url, '/room-message');
      assert.ok(!callback);
    });

    it('should reject the param signature (object) when pattern exists and event!=room_message', function () {
      var webhook, callback;
      var builder = Builder({}, {
        addWebhook: function (definition, cb) {
          webhook = definition;
          callback = cb;
        },
        ready: function () {}
      });
      try {
        builder.webhook({
          event: 'room_enter',
          name: 'name',
          pattern: /^\/foo(?:\s(.*)|$)/i,
          url: '/room-enter'
        });
        assert.fail();
      } catch (e) {
        assert.ok(/pattern not supported/.test(e.message));
      }
    });

    it('should accept the param signature (object, callback) and report the callback when no url is given', function () {
      var webhook, callback;
      var builder = Builder({}, {
        addWebhook: function (definition, cb) {
          webhook = definition;
          callback = cb;
        },
        ready: function () {}
      });
      builder.webhook({
        event: 'room_message',
        name: 'name',
        pattern: /^\/foo(?:\s(.*)|$)/i
      }, function *() {});
      assert.equal(webhook.event, 'room_message');
      assert.equal(webhook.name, 'name');
      assert.equal(webhook.pattern.toString(), /^\/foo(?:\s(.*)|$)/i.toString());
      assert.ok(callback);
    });

    it('should accept the param signature (object, callback) but not report the callback when a url is given', function () {
      var webhook, callback;
      var builder = Builder({}, {
        addWebhook: function (definition, cb) {
          webhook = definition;
          callback = cb;
        },
        ready: function () {}
      });
      builder.webhook({
        event: 'room_message',
        name: 'name',
        pattern: /^\/foo(?:\s(.*)|$)/i,
        url: '/room-message'
      }, function *() {});
      assert.equal(webhook.event, 'room_message');
      assert.equal(webhook.name, 'name');
      assert.equal(webhook.pattern.toString(), /^\/foo(?:\s(.*)|$)/i.toString());
      assert.ok(!callback);
    });

  });

  describe('#configurable', function () {

    it('should accept a path value', function (done) {
      var descriptor;
      var builder = Builder({}, {
        addWebhook: function () {},
        ready: function (built) {
          descriptor = built;
          assert.ok(descriptor.capabilities.configurable);
          assert.equal(descriptor.capabilities.configurable.url, '/configure');
          done();
        }
      });
      builder.configurable('/configure');
    });

  });

});

var assert = require('assert');
var transform = require('..').transform;

describe('ac hipchat descriptor transform', function () {

  describe('descriptor with capabilities', function () {
    var options = {
      name: 'test-add-on',
      displayName: 'Test Add-on',
      description: 'A test add-on',
      version: '0.0.1',
      author: {
        name: 'Atlassian',
        url: 'http://atlassian.com'
      }
    };
    var urls = {
      base: 'http://localhost:3000',
      homepage: '/',
      descriptor: '/addon/capabilities',
      installable: '/addon/installable'
    };
    var descriptor = {
      links: {
        self: '/addon/capabilities',
        homepage: '/'
      },
      capabilities: {
        installable: {
          callbackUrl: '/addon/installable'
        },
        configurable: {
          url: '/configure'
        }
      }
    };

    it('should populate defaults from options and urls', function () {
      var descriptor = {
        capabilities: {}
      };
      assert.deepEqual(transform(descriptor, options, urls), {
        key: 'test-add-on',
        name: 'Test Add-on',
        description: 'A test add-on',
        version: '0.0.1',
        vendor: {
          name: 'Atlassian',
          url: 'http://atlassian.com'
        },
        links: {
          self: 'http://localhost:3000/addon/capabilities',
          homepage: 'http://localhost:3000/'
        },
        capabilities: {
          hipchatApiConsumer: {
            scopes: []
          },
          installable: {
            allowGlobal: false,
            allowRoom: false,
            callbackUrl: 'http://localhost:3000/addon/installable'
          }
        }
      });
    });

    it('should make path urls full urls', function () {
      var descriptor = {
        links: {
          self: '/addon/capabilities',
          homepage: '/'
        },
        capabilities: {
          installable: {
            callbackUrl: '/addon/installable'
          },
          webhook: [{
            event: 'room_enter',
            url: '/addon/webhook'
          }],
          configurable: {
            url: '/configure'
          }
        }
      };
      assert.deepEqual(transform(descriptor, options, urls), {
        key: 'test-add-on',
        name: 'Test Add-on',
        description: 'A test add-on',
        version: '0.0.1',
        vendor: {
          name: 'Atlassian',
          url: 'http://atlassian.com'
        },
        links: {
          self: 'http://localhost:3000/addon/capabilities',
          homepage: 'http://localhost:3000/'
        },
        capabilities: {
          hipchatApiConsumer: {
            scopes: []
          },
          installable: {
            allowGlobal: false,
            allowRoom: false,
            callbackUrl: 'http://localhost:3000/addon/installable'
          },
          webhook: [{
            event: 'room_enter',
            name: '90183e0516adf37661caa9efdb79c8d1472f992a',
            url: 'http://localhost:3000/addon/webhook?name=90183e0516adf37661caa9efdb79c8d1472f992a'
          }],
          configurable: {
            url: 'http://localhost:3000/configure'
          }
        }
      });
    });

    it('should transform commands', function () {
      options.command = [{
        name: '/new_name-command',
        usage: '[usage]',
        description: 'description',
        aliases: [
          '/some_alias'
        ]
      }];
      assert.deepEqual(transform(descriptor, options, urls), {
        key: 'test-add-on',
        name: 'Test Add-on',
        description: 'A test add-on',
        version: '0.0.1',
        vendor: {
          name: 'Atlassian',
          url: 'http://atlassian.com'
        },
        links: {
          self: 'http://localhost:3000/addon/capabilities',
          homepage: 'http://localhost:3000/'
        },
        capabilities: {
          hipchatApiConsumer: {
            scopes: []
          },
          installable: {
            allowGlobal: false,
            allowRoom: false,
            callbackUrl: 'http://localhost:3000/addon/installable'
          },
          command: [{
            name: '/new_name-command',
            usage: '[usage]',
            description: 'description',
            aliases: [
              '/some_alias'
            ]
          }],
          configurable: {
            url: 'http://localhost:3000/configure'
          }
        }
      });
      delete options.command;
    });

    it('should transform commands if commands is object', function () {
      options.command = {
        name: '/new_name-command',
        usage: '[usage]',
        description: 'description',
        aliases: [
          '/some_alias',
          '/some_alias-other'
        ]
      };
      assert.deepEqual(transform(descriptor, options, urls), {
        key: 'test-add-on',
        name: 'Test Add-on',
        description: 'A test add-on',
        version: '0.0.1',
        vendor: {
          name: 'Atlassian',
          url: 'http://atlassian.com'
        },
        links: {
          self: 'http://localhost:3000/addon/capabilities',
          homepage: 'http://localhost:3000/'
        },
        capabilities: {
          hipchatApiConsumer: {
            scopes: []
          },
          installable: {
            allowGlobal: false,
            allowRoom: false,
            callbackUrl: 'http://localhost:3000/addon/installable'
          },
          command: [{
            name: '/new_name-command',
            usage: '[usage]',
            description: 'description',
            aliases: [
              '/some_alias',
              '/some_alias-other'
            ]
          }],
          configurable: {
            url: 'http://localhost:3000/configure'
          }
        }
      });
      delete options.command;
    });

    it('should throw error if command format is wrong', function () {
      options.command = [{
            name: '/wrong_name- command',
            usage: '[usage]',
            description: 'description'
      }];
      assert.throws(function () {
        transform(descriptor, options, urls);
      }, /Command name format is wrong:/);
      options.command = [{
        name: 'wrong_name-command',
        usage: '[usage]',
        description: 'description'
      }];
      assert.throws(function () {
        transform(descriptor, options, urls);
      }, /Command name format is wrong:/);
      options.command = [{
        name: '/wrong_name- command',
        usage: '[usage]',
        description: 'description'
      }];
      assert.throws(function () {
        transform(descriptor, options, urls);
      }, /Command name format is wrong:/);
      delete options.command;
    });

    it('should throw error if command alias format is wrong', function () {
      options.command = [{
        name: '/command',
        usage: '[usage]',
        description: 'description',
        aliases: [
          'wrong_alias'
        ]
      }];
      assert.throws(function () {
        transform(descriptor, options, urls);
      }, /Command alias format is wrong:/);
      delete options.command;
    });

    it('should throw error if command name or description are not defined', function () {
      options.command = [{
        name: '/some_name-command',
        usage: '[usage]'
      }];
      assert.throws(function () {
        transform(descriptor, options, urls);
      }, /Invalid string/);
      delete options.command;
      options.command = [{
        description: 'some description',
        usage: '[usage]'
      }];
      assert.throws(function () {
        transform(descriptor, options, urls);
      }, /Invalid string/);
      delete options.command;
      options.command = [{
        name: '',
        description: 'description',
        usage: '[usage]'
      }];
      assert.throws(function () {
        transform(descriptor, options, urls);
      }, /Invalid string/);
      delete options.command;
      options.command = [{
        description: '',
        name: '/name',
        usage: '[usage]'
      }];
      assert.throws(function () {
        transform(descriptor, options, urls);
      }, /Invalid string/);
      delete options.command;
    });

    it('should not overwrite explicit values', function () {
      var descriptor = {
        key: 'custom-key',
        name: 'Custom Name',
        description: 'A custom description',
        version: '1.1.1',
        vendor: {
          name: 'Custom Vendor',
          url: 'http://custom.com'
        },
        links: {
          self: 'http://custom.com/addon/capabilities',
          homepage: 'http://custom.com'
        },
        capabilities: {
          hipchatApiConsumer: {
            scopes: ['send_notification']
          },
          installable: {
            allowGlobal: true,
            allowRoom: true,
            callbackUrl: 'http://custom.com/installable'
          },
          configurable: {
            url: 'http://custom.com/configure'
          }
        }
      };
      assert.deepEqual(transform(descriptor, options, urls), descriptor);
    });
  });

});

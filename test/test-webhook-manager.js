var assert = require('assert');
var MemoryStore = require('ac-node').MemoryStore;
var MockHttpClient = require('./mock-http-client');
var RestClient = require('..').RestClient;
var WebhookManager = require('ac-node').WebhookManager;
var HipchatWebhookManager = require('..').WebhookManager;
var fixtures = require('./fixtures');

describe('ac hipchat webhook manager', function () {

  var tenant = fixtures.load('tenant.json');
  var webhookStore;
  var manager;

  beforeEach(function () {
    var parent = WebhookManager();
    var store = MemoryStore();
    var tokenStore = store.narrow('token');
    webhookStore = store.narrow('webhook');
    var httpClient = MockHttpClient(10);
    var client = RestClient(httpClient).forTenant(tenant, tokenStore, [
      'admin_group',
      'admin_room',
      'manage_rooms',
      'send_message',
      'send_notification',
      'view_group'
    ]);
    var baseUrl = 'https://example.com';
    var webhookPath = '/webhook';
    manager = HipchatWebhookManager(parent, webhookStore, tenant, client, baseUrl, webhookPath);
  });

  // TODO: test fallback to parent
  // TODO: spy on rest client methods

  it('should return nothing when get is called with an unrecognized name', function *() {
    var output = yield manager.get('missing');
    assert.ok(!output);
  });

  it('should fail on add when the name is null', function *() {
    try {
      yield manager.add(null, {});
      assert.fail();
    } catch (err) {
      assert.equal(err.message, 'Invalid string');
    }
  });

  it('should generate a full webhook definition from basic inputs', function *() {
    var input = yield manager.add('room_enter');
    assert.deepEqual(input, {
      id: 123,
      event: 'room_enter',
      url: 'https://example.com/webhook?token=3632aafe93452c1e3a7ce1eccdf266003869bf81&name=5787ac6166cc875ffc3e482d91c12703df9362e8',
      name: '5787ac6166cc875ffc3e482d91c12703df9362e8'
    });
  });

  it('should successfully get previously added definitions', function *() {
    var input = yield manager.add('room_enter');
    var output = yield manager.get(input.name);
    assert.deepEqual(input, output);
  });

  it('should successfully remove previously added definitions', function *() {
    var input = yield manager.add('room_enter');
    var output = yield manager.get(input.name);
    assert.deepEqual(input, output);
    yield manager.remove(input.name);
    output = yield manager.get(input.name);
    assert.ok(!output);
  });

});

var _ = require('lodash');
var check = require('check-types');
var verify = check.verify;

module.exports = function (nodeEnv, localBaseUrl, servicesFactory) {
  verify.string(nodeEnv);
  verify.webUrl(localBaseUrl);
  verify.fn(servicesFactory);
  return function (tenant, authToken) {
    var decoration = {};
    var services = servicesFactory(tenant);
    var tenantBaseUrl = tenant.links.base;
    _.extend(decoration, services);
    decoration.locals = {
      localBaseUrl: localBaseUrl,
      tenantBaseUrl: tenantBaseUrl,
      tenantScriptUrl: tenantAsset(tenantBaseUrl, 'js', nodeEnv),
      tenantStylesheetUrl: tenantAsset(tenantBaseUrl, 'css', nodeEnv),
      authToken: authToken
    };
    return decoration;
  };
};

function tenantAsset(tenantBaseUrl, extension, nodeEnv) {
  var isProd = nodeEnv === 'production';
  return tenantBaseUrl + '/atlassian-connect/all' + (isProd ? '' : '-debug') + '.' + extension;
}

var check = require('check-types');
var verify = check.verify;
var resolve = require('url').resolve;
var clone = require('clone');
var wh = require('./webhooks');

var COMMAND_REGEXP = /^\/[a-z_-]+$/;

var transforms = [
  metas,
  links,
  scopes,
  installable,
  configurable,
  webhooks,
  commands,
  glances,
  webPanels,
  actions,
  dialogs,
  externalPages
];

module.exports = function (descriptor, options, urls) {
  verify.object(descriptor.capabilities);
  verify.webUrl(urls.base);
  descriptor = clone(descriptor);
  transforms.forEach(function (transform) {
    descriptor = transform(descriptor, options, urls);
  });
  return descriptor;
};

module.exports.use = function (transform) {
  transforms.push(transform);
};

function metas(descriptor, options, urls) {
  if (!descriptor.key && options.name) {
    descriptor.key = options.name;
  }
  if (!descriptor.name && options.displayName) {
    descriptor.name = options.displayName;
  }
  if (!descriptor.description && options.description) {
    descriptor.description = options.description;
  }
  if (!descriptor.version && options.version) {
    descriptor.version = options.version;
  }
  var vendor;
  if ((!descriptor.vendor || !descriptor.vendor.name) && options.author) {
    vendor = descriptor.vendor = descriptor.vendor || {};
    if (typeof options.author === 'string') {
      vendor.name = options.author;
    } else if (options.author.name) {
      vendor.name = options.author.name;
    }
  }
  if ((!descriptor.vendor || !descriptor.vendor.url) && options.author && options.author.url) {
    vendor = descriptor.vendor = descriptor.vendor || {};
    vendor.url = options.author.url;
  }
  return descriptor;
}

function links(descriptor, options, urls) {
  var links = descriptor.links = descriptor.links || {};
  links.self = ensureUrl(links.self, urls.base, urls.descriptor);
  links.homepage = ensureUrl(links.homepage, urls.base, urls.homepage);
  return descriptor;
}

function scopes(descriptor, options, urls) {
  if (!descriptor.capabilities.hipchatApiConsumer) {
    descriptor.capabilities.hipchatApiConsumer = {scopes: []};
  }
  return descriptor;
}

function installable(descriptor, options, urls) {
  var capabilities = descriptor.capabilities;
  var installable = capabilities.installable = capabilities.installable || {};
  if (!installable.allowGlobal) {
    installable.allowGlobal = false;
  }
  if (!installable.allowRoom) {
    installable.allowRoom = false;
  }
  installable.callbackUrl = ensureUrl(installable.callbackUrl, urls.base, urls.installable);
  return descriptor;
}

function configurable(descriptor, options, urls) {
  var capabilities = descriptor.capabilities;
  var configurable = capabilities.configurable;
  if (configurable) {
    if (!configurable.url || !check.string(configurable.url)) {
      delete capabilities.configurable;
    } else if (!check.webUrl(capabilities.configurable.url)) {
      configurable.url = buildUrl(urls.base, configurable.url);
    }
  }
  return descriptor;
}

function webhooks(descriptor, options, urls) {
  var capabilities = descriptor.capabilities;
  if (check.array(capabilities.webhook)) {
    capabilities.webhook = capabilities.webhook.map(function (webhook) {
      return wh.normalize(webhook, urls.base, urls.webhook);
    });
  }
  return descriptor;
}

function commands(descriptor, options) {

  var commands  = options.command;

  if (commands){
    if (!check.array(commands)){
      commands = [commands];
    }

    var capabilities = descriptor.capabilities;
    capabilities.command = commands.map(function (command) {
      check.map(command, {
        name: verify.unemptyString,
        description: verify.unemptyString,
        usage: verify.maybe.string,
        aliases: verify.maybe.array
      });
      if (!COMMAND_REGEXP.test(command.name)){
        throw new Error('Command name format is wrong: ' + command.name);
      }
      if (command.aliases){
        for (var key in command.aliases) {
          var alias = command.aliases[key];
          if (!COMMAND_REGEXP.test(alias)) {
            throw new Error('Command alias format is wrong: ' + alias);
          }
        }
      }
      return {
        name: command.name,
        usage: command.usage,
        description: command.description,
        aliases: command.aliases
      }
    });
  }

  return descriptor;
}

function glances(descriptor, options, urls) {
  var capabilities = descriptor.capabilities;
  if (check.array(capabilities.glance)) {
    capabilities.glance = capabilities.glance.map(function (glance) {
      glance.queryUrl = buildUrl(urls.base, glance.queryUrl);
      glance.icon["url"] = buildUrl(urls.base, glance.icon["url"]);
      glance.icon["url@2x"] = buildUrl(urls.base, glance.icon["url@2x"]);
      return glance;
    })
  }
  return descriptor;
}

function webPanels(descriptor, options, urls) {
  var capabilities = descriptor.capabilities;
  if (check.array(capabilities.webPanel)) {
    capabilities.webPanel = capabilities.webPanel.map(function (webPanel) {
      webPanel.url = buildUrl(urls.base, webPanel.url);
      return webPanel;
    })
  }
  return descriptor;
}

function actions(descriptor, options, urls) {
  var capabilities = descriptor.capabilities;
  if (check.array(capabilities.action)) {
    capabilities.action = capabilities.action.map(function (action) {
      action.url = buildUrl(urls.base, action.url);
      return action;
    })
  }
  return descriptor;
}

function dialogs(descriptor, options, urls) {
  var capabilities = descriptor.capabilities;
  if (check.array(capabilities.dialog)) {
    capabilities.dialog = capabilities.dialog.map(function (dialog) {
      dialog.url = buildUrl(urls.base, dialog.url);
      return dialog;
    })
  }
  return descriptor;
}

function externalPages(descriptor, options, urls) {
  var capabilities = descriptor.capabilities;
  if (check.array(capabilities.externalPage)) {
    capabilities.externalPage = capabilities.externalPage.map(function (externalPage) {
      externalPage.url = buildUrl(urls.base, externalPage.url);
      return externalPage;
    })
  }
  return descriptor;
}

function ensureUrl(value, base, def) {
  if (!value && def) {
    value = buildUrl(base, def);
  } else if (check.string(value) && !check.webUrl(value)) {
    value = buildUrl(base, value);
  }
  return value;
}

function buildUrl(base, path) {
  return (base + '/' + path).replace(/([^:])\/{2,3}/g, '$1/');
}
